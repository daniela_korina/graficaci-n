var morris1 = new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'myfirstchart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
    { year: '2010', value: 20, value2: 30 },
    { year: '2011', value: 10, value2: 40 },
    { year: '2012', value: 5, value2: 10 },
    { year: '2013', value: 8, value2: 4 },
    { year: '2014', value: 20, value2: 25 },
    { year: '2015', value: 5, value2: 20 },
    { year: '2016', value: 25, value2: 13 }
  ],
  // The name of the data record attribute that contains x-values.
  xkey: 'year',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value', 'value2'],
  lineWidth: 1,
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Coca Cola', 'Pepsi'],
  resize: true,
  lineColors: ['#C14D9F','#2CB4AC']
});


// Use Morris.Area instead of Morris.Line
var morris2 = new Morris.Area({
  element: 'GraficaClientes',
  data: [
    {x: '2010', y: 3, z: 7},
    {x: '2011', y: 3, z: 4},
    {x: '2012', y: 2, z: 0},
    {x: '2013', y: 2, z: 5},
    {x: '2014', y: 8, z: 2},
    {x: '2015', y: 4, z: 4},
    {x: '2016', y: 4, z: 4}
  ],
  xkey: 'x',
  ykeys: ['y', 'z'],
  labels: ['Coca Cola', 'Pepsi'],
  lineColors: ['#C14D9F','#2CB4AC'],
  resize: true,
  lineWidth: 1,

});


$("#botData").on("click", function(){

  console.log( morris1 );

  var nuevaData = [
    { year: '2010', value: 10, value2: 17 },
    { year: '2011', value: 20, value2: 30 },
    { year: '2012', value: 15, value2: 5 },
    { year: '2013', value: 4, value2: 10 },
    { year: '2014', value: 40, value2: 25 },
    { year: '2015', value: 25, value2: 20 },
    { year: '2016', value: 15, value2: 30 }
  ];

  morris1.setData(nuevaData);

});

$("#botData").on("click", function(){

  console.log( morris2 );

  var nuevaData2 = [
    {x: '2010', y: 5, z: 3},
    {x: '2011', y: 5, z: 2},
    {x: '2012', y: 7, z: 1},
    {x: '2013', y: 8, z: 5},
    {x: '2014', y: 2, z: 4},
    {x: '2015', y: 5, z: 7},
    {x: '2016', y: 6, z: 2}
  ];

  morris2.setData(nuevaData2);

});